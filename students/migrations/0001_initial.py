# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='students',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('mobile_no', models.CharField(max_length=10)),
                ('college', models.CharField(max_length=20)),
                ('college_id', models.CharField(max_length=10)),
            ],
        ),
    ]
