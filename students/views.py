from django.views.generic import View
from django.http import HttpResponse, QueryDict
from django.core.exceptions import ObjectDoesNotExist
import json
from . models import Student


#for arithmetic operations 
class ArithmeticOperator(View):
	#get :fetch the result
	def get(self,request):
		param1 = int(request.GET['p1'])
		param2 = int(request.GET['p2'])
		data={}
		data['add'] = param1 + param2
		data['subtract'] = param1 - param2
		data['multiply'] = param1 * param2
		data['division'] = param1 / param2
		return HttpResponse("The result :"+ json.dumps(data) ,status=200)


#for getting info about students in the internship
class StudentData(View):

	#get: Fetch the details of students
	def get(self ,request):
		details = []
		students = Student.objects.all()
		for i in students:
			res = {}
			res['name'] = i.name
			res['mobile_no'] = i.mobile_no
			res['college'] = i.college
			res['college_id'] = i.college_id
			details.append(res)
		return HttpResponse(details,status=200)	

	#post: add students
	def post(self ,request):
		data = request.POST
		add_obj = Student(name=data.get('name'),mobile_no=data.get('mobile_no'),college=data.get('college'),
					college_id=data.get('college_id'))
		add_obj.save()
		return HttpResponse("added Successfully")

	#delete: delete student	
	def delete(self, request,id):
		try:
			delete_obj=Student.objects.get(id=id)
			delete_obj.delete()
			return HttpResponse("deleted Successfully")
		except ObjectDoesNotExist:
			return HttpResponse("doesn't exist")	

	#put: update the details of student		
	def put(self ,request):
		data = QueryDict(request.body)
		try:
			update_obj = Student.objects.get(college_id=data.get('college_id'))
			if data.get('name') is not None :
				update_obj.name = data.get('name')
			if data.get('mobile_no') is not None :
				update_obj.mobile_no = data.get('mobile_no')
			update_obj.save()
			return HttpResponse("updated Successfully")
		except ObjectDoesNotExist:
			return HttpResponse("college_id :"+data.get('college_id')+" doesn't exist")

		