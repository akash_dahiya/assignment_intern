from django.db import models

# Student model
class Student(models.Model):
	name=models.CharField(max_length=255)
	mobile_no=models.CharField(max_length=255)
	college=models.CharField(max_length=255)
	college_id=models.CharField(max_length=255,unique=True)