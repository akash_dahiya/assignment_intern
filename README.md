#Project name: internship
this project is used for get,insert,delete,update the student details by GET,POST,DELETE,PUT requests.

##App name: students

students_model - name,mobile_no,college,college_id.

I have create api's "student_Details" and "student_details/?P<id>\d+" for 
getting details(Add,update,fetch) and deleting a specific object of student respectively.

Methods:

      - get method:
        To fetch the details of the student.
      - post method:
        To insert the details of the students table.
      - delete method:
        To delete the details of a student.
      - put method:
        To update the details of a student.