from django.conf.urls import include, url
from django.contrib import admin
from students.views import StudentData,ArithmeticOperator
from django.views.decorators.csrf import csrf_exempt


urlpatterns = [

    url(r'^admin/', include(admin.site.urls)),
    url(r'^student_details/$',csrf_exempt(StudentData.as_view()),name='students details'),
    url(r'^student_details/(?P<id>\d+)/$',csrf_exempt(StudentData.as_view()),name='students'),
    url(r'^arithmetic/',csrf_exempt(ArithmeticOperator.as_view()),name='arithmetic operator'),
]
